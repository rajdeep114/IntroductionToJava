import java.util.*;

public class Gradenatormodified{
   public static void main(String [] args){
      Scanner console = new Scanner(System.in);
      Grade(console);
      
   }
   
   // Intro
   public static void intro(){
      System.out.println("This program readss exam/homework scores\nand reports" +
                                 " your overall course grade\n");
   }
  
   // Overall Calculation
   public static void Grade(Scanner console){
     double Overall_percent = theme(console,"Midterm") + theme(console,"Final") + Homework(console);
     System.out.printf("Overall percentage =  %.1f\n", Overall_percent);
     GPA(Overall_percent);
     System.out.print("Best of Luck !!");
   }
   
   // Basic Stucture for Midterm and Final
   public static double theme(Scanner console, String s){
      int max = 100; 
      System.out.print(s + ":\nWeight (0-100) ? ");
      int weight = console.nextInt();
      System.out.print("Score earned? ");
      int score = console.nextInt();
      System.out.print("Were scores shifted (1=yes. 2=no) ? ");
      int score_shifted = console.nextInt();
      if(score_shifted == 1){
         System.out.print("Shift amount? ");
         int shifted_amount = console.nextInt();
         score = score + shifted_amount;
      }
      if (score > max){
         score = Math.min(score,max);
      }
      double WeightedScore = weighted(score, max, weight) ;
      
      System.out.println("Total Points = " + score + " / " + max );
      System.out.println("Weighted Score = " + WeightedScore + " / " + weight + "\n" );
      return WeightedScore;
   }
   
   // Homework Score
   public static double Homework(Scanner console){
      System.out.print("Homework:\nWeight (0-100)? "); 
      int weight = console.nextInt();
      System.out.print("Number of Assignments? ");
      int assignments = console.nextInt();
      int total_score = 0;
      int max = 0;
      for(int i = 1; i <= assignments; i++){
         System.out.print("Assignment " + i + " score and max? ");
         int score = console.nextInt();
         int max_score = console.nextInt();
         total_score = total_score + score;
         max = max + max_score;
      }
      
      System.out.print("How many sections did you attend? ");
      int sec = console.nextInt();
      int sec_points = sec * 5;
      int max_sec_points = 30;
      if (sec_points > max_sec_points){
         sec_points = Math.min(sec_points,max_sec_points);
      }
      max = max + max_sec_points;
      total_score = total_score + sec_points;
      if (total_score > max){
         total_score = Math.min(total_score,max);
      }   
      
      double WeightedHomeworkScore = weighted(total_score,max,weight);
      System.out.println("Sections points = " + sec_points + " / " + max_sec_points);
      System.out.println("Total Points = " + total_score + " / " + max );
      System.out.printf("Weighted Score =  %.1f / " + weight + "\n\n",WeightedHomeworkScore);
      return WeightedHomeworkScore;
   }
   
   // GPA Decision Statements
   public static void GPA(double Overall_percent){
      if (Overall_percent >= 85.0){
         System.out.println("Your grade will be at least: 3.0");
      } else if (Overall_percent >= 75.0){
         System.out.println("Your grade will be at least: 2.0");
      } else if (Overall_percent >= 60.0){
         System.out.println("Your grade will be at least: 0.7");
      } else {
         System.out.println("Your grade will be at least: 0.0");   
   
      }
   }
   
   // Method to calculate weighted score
   public static double weighted(int score_earned, int max, int weight){
      return  roundoff(((double)score_earned * weight) / max);  
   } 
   
    public static double roundoff(double values){
      return   Math.round(10.0 * values)/10.0;
      
   }  
}      
      