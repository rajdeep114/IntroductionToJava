import java.util.*;
import java.io.*;

public class DNA {
   public static void main(String [] args) throws FileNotFoundException {
      Scanner console = new Scanner(System.in);
      String[] response = Intro(console);
      Scanner file = new Scanner(new File(response[0]));
      PrintStream s = new PrintStream(response[1]);
      while(file.hasNextLine()){
         String region = file.nextLine();
         String line = file.nextLine();
         int dashes = countDashes(line);
         int[] count = counts(line);
         double averageMass = average(count, dashes);
         double[] massPercent = massPercent(count,averageMass);
         String[] codonsList = codonsList(line);                    
                  String protein = isProtein(codonsList, count, averageMass);                   
         print(file, s, region,line, count, averageMass, massPercent, 
               codonsList, protein);
      }
     
   }
   public static String[] Intro(Scanner console){
      System.out.print("This program reports information about DNA\n" +
                        "nucleotide sequences that may encode proteins\n");
      System.out.print("Input file name? ");
      String input = console.nextLine();
      System.out.print("Output file name? ");
      String output = console.nextLine(); 
      String[] response = {input, output};
      return response;                 
   }                     
                        
   public static void print(Scanner file, PrintStream s, String region,
                            String line, int[] count, double averageMass,
                            double[] massPercent, String[] codonsList, String protein) {
      s.println("Region Name: " + region);
      s.println("Nucleotides: " + line);
      s.println("Nuc. Counts: " + Arrays.toString(count));
      s.println("Total Mass%: " + Arrays.toString(massPercent) +
                              " of " + Math.round(10.0 * averageMass) / 10.0);
      s.println("Codons List: " + Arrays.toString(codonsList));                        
      s.println("Is Protein?: " + protein +  "\n"); 
   
   }
      
   public static String isProtein(String[] a, int[] count, double averageMass) {
      String protein = "NO";
      double A = ((count[0]*135.128)/averageMass) * 100;
      double B = ((count[1]*111.103)/averageMass) * 100;
      double sumAB = A + B;
      if(a[0].equals("ATG") && (a[a.length - 1].equals("TAA") 
           || a[a.length - 1].equals("TAG")|| a[a.length - 1].equals("TGA")) 
           && a.length >= 5 && sumAB >= 30) {
         protein = "YES";
      }      
      return protein;                                
   }      
   
   public static String[] codonsList(String line) {
      line = line.replace("-","");
      String[] codons = new String[line.length()/3];
      for(int i = 0; i < line.length() / 3; i++) {
         codons[i] = line.substring(i * 3, i*3+3).toUpperCase(); 
      }
      return codons;
   } 
    
   public static int[] counts(String line) {
      int[] counter = new int[4];
      for(int i = 0; i < line.length(); i++) {
         char nucleotide = line.charAt(i);
         if(nucleotide!= '-'){
            int index = decision(nucleotide);
            counter[index] ++;
         }     
      } 
      return counter;   
   }
   
    public static double[] massPercent(int[] count, double averageMass){
       double[] mass = {135.128, 111.103, 151.128, 125.107};
       double[] massPercent = new double[4];
       for(int i = 0; i < count.length;i++) {
          massPercent[i] = (mass[i] * count[i] / averageMass)* 100.0;
          massPercent[i] = Math.round(10.0 * massPercent[i]) / 10.0;
       }
       return massPercent;
    }
    
   public static double average(int[] count, int dashes) {
      double[] mass = {135.128, 111.103, 151.128, 125.107};
      double average = 0;
      for(int i = 0; i < mass.length; i++) {
         average += mass[i] * count[i];
      }
      average += dashes * 100.00;
      return average;   
   }   
  
   public static int countDashes(String line) {
      int dashes = 0;
      for(int i = 0; i < line.length(); i++){
         if(line.charAt(i) == '-') {
            dashes ++;
         }
      }
      return dashes;
   }
                   
   public static int decision(char nucleotide) {
      int index = 0;
     if(nucleotide == 'a'|| nucleotide == 'A')  {
         index = 0; 
      } else if (nucleotide == 'c'|| nucleotide == 'C') {
         index = 1; 
      } else if (nucleotide == 'g'|| nucleotide == 'G') {
         index = 2; 
      } else if (nucleotide == 't'|| nucleotide == 'T') {
         index = 3;  
      }
      return index;
   }    
}   
      