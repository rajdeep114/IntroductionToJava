import java.awt.*;

public class Illusion{
   public static void main(String [] args){
      DrawingPanel panel = new DrawingPanel(500,400);
      Graphics g = panel.getGraphics();
      panel.setBackground(Color.GRAY);
     
      circle_1(g,0, 0, 90, 3);
      circle_1(g,120, 10, 90, 3);
      circle_1(g,250, 50, 80, 5);

      repeat(g,10,120,200,100,10,2,2);
      repeat(g,350,20,120,40,5,3,3);
      repeat(g,230,160,200,50,5,4,4);
   }
   
   public static void circle_1(Graphics a, int x, int y, int size, int c){
  
      a.setColor(Color.RED);
      a.fillOval(x, y, size, size);
      a.setColor(Color.BLACK); 
           
      for (int i = 1; i <= c; i++){
         a.drawOval(x + (size - (size * i)/c)/2, y + (size - (size * i)/c) / 2, 
         ((size * i)/c), (size * i)/c);
      }
      a.drawLine(x, y + size / 2, x + size / 2, y);
      a.drawLine(x + size / 2, y, x + size, y + size / 2 );
      a.drawLine(x + size, y + size / 2, x + size / 2, y + size);
      a.drawLine(x + size / 2, y + size, x, y + size / 2);
   
   }
   
   public static void repeat(Graphics a, int x, int y, int rectsize, int size,
                              int c,int rows, int columns ){
   
      a.setColor(Color.LIGHT_GRAY);
      a.fillRect(x,y,rectsize,rectsize);
      a.setColor(Color.RED);
      a.drawRect(x, y, rectsize, rectsize);
      
    
      for(int col = 1; col <= rows; col++){   
         for(int r = 1; r <= columns; r++){
          
         
            circle_1(a, size * (r - 1) + x,size * (col - 1) + y, size,c);
         }
      }
   }
}

     /* a.setColor(Color.RED);
            a.fillOval((r * size - size) + x,(col * size - size) + y, size, size);
            a.setColor(Color.BLACK);
         
            for (int i = 1; i <= c; i++){
               a.drawOval((r * size - size) + x + (size - (size * i)/c)/2,(col * size - size) + y + (size - (size * i)/c) / 2, ((size * i)/c), (size * i)/c);
            }
            a.drawLine((r * size - size)+ x, (col * size - size) + y + size / 2, (r * size - size) + x + size / 2,(col * size - size)+ y);
            a.drawLine((r * size - size) + x + size / 2, (col * size - size) + y, (r * size - size) + x + size, (col * size - size) + y + size / 2 );
            a.drawLine((r * size - size) + x + size, (col * size - size) + y + size / 2, (r * size - size) + x + size / 2, (col * size - size) + y + size);
            a.drawLine((r * size - size) + x + size / 2,(col * size - size) + y + size, (r * size - size) + x, (col * size - size)+ y + size / 2);
         }*/
      
     // }
      
         /*    a.setColor(Color.RED);
      a.fillOval(0, 0, 90, 90);
      a.setColor(Color.BLACK);
      a.drawOval(0, 0, 90, 90);
      a.drawOval(15, 15, 60, 60);
      a.drawOval(30, 30, 30, 30);
      a.drawLine(0, 45, 45, 0);
      a.drawLine(45, 0, 90, 45 );
      a.drawLine(90, 45, 45, 90);
      a.drawLine(45, 90, 0, 45);
   */
