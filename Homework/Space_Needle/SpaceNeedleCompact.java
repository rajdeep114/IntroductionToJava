import java.util.*;

public class SpaceNeedleCompact{
   public static void main(String [] args){
      Scanner type = new Scanner(System.in);
      System.out.print(" What is the space needle size? ");
      int Size = type.nextInt();
      mainframe(Size);
         
    }
    
    /*   In this program we programmed a compact version of spaceneedle, which was a homework 2. The basic idea of this code is that we
         make a new method that takes in two inputs, namely type of string and number of time we want to repeat that string. This help us 
         reduce the redundancy in our previous code of spaceneedle.In this new method we create a for loop that repeats itself "times" time,
         which in our case were already define in the previous code. In this for loop, we print the Sring s defined per our requirement.
         
         In order to make the code work, we comment out the final constant x declared in the code. Then, for all the method we created previously 
         are parameterized using 'int Size'. This means that when we call individual methods, we need to specify the size of the individual segments
         of the spaceneedle. In order to make spaceneedle look good and symmetric, we pass the same parameter called size to all methods.
         
         In order to modify the code, we replace the inner loop by calling the new method created that take in two parameters. We, however, still
         need the outer for loop since it prints output vertically. Now java would repeatedly call the method again and again until the program
         exits the outer for loop. This will give us required output.
         
         We also used console input in order to make our program more interactive. We used Scanner class in our program and made a new scanner
         named type. 
    
    */
    
    public static void mainframe(int Size){ 
      Needle(Size);
      Top_part1(Size);
      Line(Size);
      Top_part2(Size);
      Needle(Size);
      Middle(Size);
      Top_part1(Size);
      Line(Size);
    }
    
    public static void Needle(int Size){
         for(int Line = 1; Line <= Size; Line++){
            p(" ",3 * Size );
            System.out.print("||");
            p(" ",3 * Size );
            System.out.println();
        }
    }
     
    public static void Top_part1(int Size){
         for(int Line = 1; Line <= Size; Line ++){
            p(" ",-3 * Line + Size * 3 );
            System.out.print("__/");
            p(":",3 * Line - 3);
            System.out.print("||");
            p(":",3 * Line - 3);
            System.out.print("\\__");
            p(" ",-3 * Line + Size * 3 );
            System.out.println();
          }
      }
      
      public static void Line(int Size){
         System.out.print("|");
         p("\"",Size * 6);
         System.out.println("|");
     }
     
     public static void Top_part2(int Size){
         for(int Line = 1; Line <= Size; Line ++){
            p(" ",2 * Line - 2);
            System.out.print("\\_/");
            p("\\/", -2 * Line + Size * 3);
            System.out.print("\\_/");
            p(" ",2 * Line - 2);
            System.out.println(); 
          }
       } 
       
     public static void Middle(int Size){
         for(int Line = 1; Line <= 4 * Size; Line++){
            p(" ",2 * Size + 1);
            System.out.print("|");
            p("%",Size - 2);
            System.out.print("||");
            p("%",Size - 2);
            System.out.print("|");
            p(" ",2 * Size + 1);
            System.out.println();
         }           
     }
     
     public static void p(String s, int times){
         for (int i = 1; i <= times; i++){
            System.out.print(s);
         }        
     }           
 }
       
       
       
     
           
         
         
         