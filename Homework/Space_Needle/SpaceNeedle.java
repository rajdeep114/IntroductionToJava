public class SpaceNeedle{
   public static final int Size = 4;
   public static void main(String [] args){
     
         Needle();
         Top_part1();
         Line();
         Top_part2();
         Needle();
         Middle();
         Top_part1();
         Line();
    }
    
    public static void Needle(){
         for(int Line = 1; Line <= Size; Line++){
            for(int spaces = 1;spaces <= 3 * Size ; spaces++){
               System.out.print(" ");
            }
            System.out.print("||");
            for(int spaces = 1;spaces <= 3 * Size; spaces++){
               System.out.print(" ");
            }
            System.out.println();
        }
    }
     
    public static void Top_part1(){
         for(int Line = 1; Line <= Size; Line ++){
            for(int spaces = 1; spaces <= -3 * Line + Size * 3; spaces++){
               System.out.print(" ");
            }
            
            System.out.print("__/");
            
            for(int double_dots = 1; double_dots <= 3 * Line - 3; double_dots ++){
               System.out.print(":");
            }
            
            System.out.print("||");
            
            for(int double_dots = 1; double_dots <= 3 * Line - 3; double_dots ++){
               System.out.print(":");
            }
            
            System.out.print("\\__");
            
            for(int spaces = 1; spaces <= -3 * Line + Size * 3; spaces++){
               System.out.print(" ");
            }
            System.out.println();
          }
      }
      
      public static void Line(){
         System.out.print("|");
         for(int double_comma = 1;double_comma <= Size * 6; double_comma++){
               System.out.print("\"");
            }
         System.out.println("|");
     }
     
     public static void Top_part2(){
         for(int Line = 1; Line <= Size; Line ++){
             for(int spaces = 1; spaces <= 2 * Line - 2; spaces++){
               System.out.print(" ");
             } 
             System.out.print("\\_/");
             
             for(int cone = 1; cone <= -2 * Line + Size * 3; cone++){
               System.out.print("\\/");
             } 
             
             System.out.print("\\_/");
             
              for(int spaces = 1; spaces <= 2 * Line - 2; spaces++){
                  System.out.print(" ");
             } 
             
             System.out.println();
             
          }
          
       }
       
     public static void Middle(){
         for(int Line = 1; Line <= 4 * Size; Line++){
             for(int spaces = 1; spaces <= 2*Size + 1; spaces++){
               System.out.print(" ");
             } 
             
             System.out.print("|");
             for (int percent = 1; percent <= Size - 2; percent++){
               System.out.print("%");
             }
             System.out.print("||");
             
             for (int percent = 1; percent <= Size - 2; percent++){
               System.out.print("%");
             }
             
             System.out.print("|");
             
             for(int spaces = 1; spaces <= 2 * Size + 1; spaces++){
               System.out.print(" ");
             }
             System.out.println();
             
         }
           
     }     
       
       
 }
       
       
       
     
           
         
         
         