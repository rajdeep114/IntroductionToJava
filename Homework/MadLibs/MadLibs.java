import java.util.*; // Scanner
import java.io.*; // Read files: Input, echo and output data

public class MadLibs{
   public static void main(String [] args) throws FileNotFoundException{
      
      Intro();                                        // Intro                                                    
      Scanner console = new Scanner(System.in);       // Scanner to read user input
      PrintStream out = System.out;                   // PrintStream for creating output files
      String play = cVQ(console);                     // call cVQ method to get return value
      while(play.equals("c") || play.equals("v")){    // Decision to be in loop
      
         // Loop for create mad libs
         if(play.equals("c")){
            Scanner file = Input(console);
            PrintStream stream = Output(console);
         
            while(file.hasNextLine()){
               String text = file.nextLine();
               createMadLib(console, text, stream);
            }
            System.out.println("Your mad-lib has been created!\n");
            
          // Loop for view madlib  
         }else if(play.equals("v")){
            Scanner file = Input(console);
            viewMadLib(file, out);
            out.println();
         }
         
         // Prompt user with (C)reate mad-lib, (V)iew madlib, (Q)uit?
         // And loop back if user decides to play or quit otherwise.
         play = cVQ(console);      
      }
   }   
   
   // Intro
   public static void Intro(){
      System.out.println("Welcome to the game of Mad Libs.");
      System.out.println("I will ask you to provide various words\nand phrases to fill in a story.");
      System.out.println("The result will be written to an output file.\n"); 
   }
   
   // Method that help us re-prompt user when response other than c,v, q are entered.
   // This method also returns a value that help user play game over and over.
   public static String cVQ(Scanner console){
      System.out.print("(C)reate mad-lib, (V)iew madlib, (Q)uit? ");
      String begin = console.nextLine();
      while(!begin.equalsIgnoreCase("c") && !begin.equalsIgnoreCase("v") && !begin.equalsIgnoreCase("q")) {
         System.out.print("(C)reate mad-lib, (V)iew madlib, (Q)uit? ");
         begin = console.nextLine();
      }
      String play = " ";
      if (begin.equalsIgnoreCase("c")){
            play = "c";
         
      }else if (begin.equalsIgnoreCase("v")){
            play = "v";
      }
       
      return play;       
   }
   
   // Method return a Scanner, which is an object.
   // This method use file not exist method to re-prompt user if file does not exist.
   // While using an File object in any method, we need FNFE, regardless of the type
   // of method.
   public static Scanner Input(Scanner console)throws FileNotFoundException{
      System.out.print("Input file name: ");
      String inputFile = console.nextLine();
      File file = new File(inputFile);
      while(!file.exists()){
         System.out.print("File not found. Try again: ");
         inputFile = console.nextLine();
         file = new File(inputFile); 
      }
      Scanner input = new Scanner(file);
      return input;
   }
    
   // Method returns a Printstream object, that is used to create an output file
   public static PrintStream Output(Scanner console)throws FileNotFoundException{
      System.out.print("Output file name: ");
      String outputFile = console.nextLine();
      PrintStream stream = new PrintStream(outputFile);
      System.out.println();
      return stream;
   }
   
   // This method helps in creating the MadLibs. 
   // It uses several string method such as replace, startsWith, ignoreCase etc
   // It takes several objects and a string as parameter.
   // It also use a vowel method to decided when to use vowels.
   
   // Scanner console --> User input,
   // String text ---> Single line provided to method
   // PrintStream ---> Output result in an external file.
                                         
   public static void createMadLib(Scanner console, String text, PrintStream s){
      Scanner line = new Scanner(text);         // New scanner to read individual 
                                                // in a line
      while(line.hasNext()){
         String token = line.next();
         if (token.startsWith("<") && token.endsWith(">")){
            token = token.replace("-"," ");
            token = token.substring(1,token.length() - 1);
            String vowel = isVowel(token.substring(0,1));
            System.out.print("Please type " + vowel + " "  + token + ": ");    
            String response = console.nextLine();
            token = token.replace(token,response);  
         }
         s.print(token + " ");
      }
      s.println();
   }
   
   // Method used for viewing madlib files in the program
   public static void viewMadLib(Scanner input, PrintStream s){
      s.println();
      while(input.hasNext()){          
         String text = input.nextLine(); 
         s.println(text);         
      }   
   }   
   
   // Method returns 'an' for vowels and 'a' otherwise
   public static String isVowel(String vowel){
      String out;
      if(vowel.equalsIgnoreCase("a") || vowel.equalsIgnoreCase("e") || 
         vowel.equalsIgnoreCase("i") || vowel.equalsIgnoreCase("o") || 
                                          vowel.equalsIgnoreCase("u")){
         return out = "an";
      } else {
         return out = "a";
      } 
   }                  
}                  