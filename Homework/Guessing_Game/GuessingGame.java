import java.util.*;

public class GuessingGame{
   public static final int max = 5;
   public static void main(String [] args){
      Scanner console = new Scanner(System.in); 
      Random r = new Random();
      
      int guess_game1 = Game(console,r);   // store guesses in game one
      
      // 'Pole', or the statement displayed that prompt user for input. 
      // while loop used to play multiple games.
      System.out.print("Do you want to play again?" );
      String answer = console.next();
      String ans = answer.substring(0,1);
      
      // Initialize varible for total games
      int totalgames = 1;
      
      // Initialize min to find min number of guesses in the entire game
      // set min to value obtain from Game 1 
      int min = guess_game1;
      
      //Initialize totalguesses variable to find total number of guesses.
      // This is done by finding cumulative sum.
      int totalguesses = guess_game1;
      
      while (ans.equalsIgnoreCase("y")){
         System.out.println();
         
         int x = Game(console,r); // x is the guess per game
         
         if ( min > x){           // finding the min guesses
             min = x;
          }   
         totalguesses = totalguesses + x; // cumulative sum
         System.out.print("Do you want to play again?" );
         answer = console.next();
         ans = answer.substring(0,1);
         totalgames++;   
      }
      
      Stats(totalguesses,totalgames);
      System.out.print("Best game       = " + min);
      }
      
   public static void Stats(int totalguesses,int totalgames){
      System.out.println("\nOverall results: ");
      System.out.println("Total games     = " + totalgames);
      System.out.println("Total guesses   = " + totalguesses);
      double average = (double)totalguesses / totalgames;
      System.out.printf("Guesses/games   = %.1f\n", average);
   } 
   

   public static int Game(Scanner console, Random r){
      System.out.println("I'm thinking of a number between 1 and " + max + " ...");
      int random_number = r.nextInt(max) + 1;
      System.out.println("Hint: " + random_number);
      System.out.print("Your guess? ");
      int guess = console.nextInt();
      int guesses = 1;
      if (guess == random_number){
         System.out.println("You got it right in " + guesses +" guess!");
      }else{
         while(guess != random_number){
            guesses ++;
            if (guess < random_number){
               System.out.println("It's Higher");
            }else {
               System.out.println("It's Lower");
            }     
            System.out.print("Your guess? ");
            guess = console.nextInt();
            } 
            System.out.println("You got it right in " + guesses +" guesses!");
      }
      return guesses;      
   }     
}    