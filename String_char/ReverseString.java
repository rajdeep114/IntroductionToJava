import java.util.*;
public class ReverseString {
   public static void main(String[] args) {
      Scanner console = new Scanner(System.in);
      System.out.print("Please enter a word: ");
      String word = console.next();
      System.out.print("Please enter a number: ");
      int integer = console.nextInt();
      boolean response1 = checkPallindrome(word);
      boolean response2 = checkPallindrome2(integer);
      printResult(response1);
      printResult(response2);
      
   }   
  
   public static String reverse(String a) {
      String result = "";
      for(int i = 0; i < a.length(); i++) {
         result = a.charAt(i) + result;
      }
      return result;
   }
   
   public static boolean checkPallindrome(String a) {
      boolean found = false;
      String reverse = reverse(a);
      if (reverse.equalsIgnoreCase(a)) {
         found = true;
      } 
      return found;
   }  
   
   public static int revInteger(int a) {
      int result = 0;
      while(a > 0){
         result = a % 10 + result * 10;
         a = a / 10;
      }
      return result;   
   } 
   
   public static boolean checkPallindrome2(int a) {
      boolean found = false;
      int reverse = revInteger(a);
      if (reverse == a) {
         found = true;
      } 
      return found;
   }
   
   public static void printResult(boolean a) {
      if(a){
         System.out.println("You enter a Pallindrome!!");
      } else {
         System.out.println("Sorry, you did not enter a Pallindrome!!");
      }
   }                
}   