import java.util.*;

public class FirstLastName{
   public static void main(String [] args){
      System.out.println(convert("Singh, Rajdeep"));
      
      Scanner console = new Scanner(System.in);
      System.out.print("Please print your name? " );
      String Name = console.nextLine();
      
      System.out.print(convert(Name));
   }
   // We just created a return method that returns a first and last name
   public static String convert(String name){
      int comma_index = name.indexOf(",");
      int total_length = name.length();
      
      String LastName = name.substring(0,comma_index);
      String FirstName = name.substring(comma_index + 2, total_length );
      
      return FirstName.toUpperCase() + "," + LastName.toUpperCase();
   }  
}      