import java.util.*;

public class Split {
   public static void main(String[] args) {
      int[] number1 = {2, 4, 6, 8, 10}; 
      int[] number2 = {1, 2, 3, 4, 5}; 
      int[] number3 = {3, 7, 9, 11, 1}; 
      int[] number4 = {0, 0, 6, 8, 10}; 
      convertToArrayList(number1);
      convertToArrayList(number2);
      convertToArrayList(number3);
      convertToArrayList(number4);  
   }
   
   public static void convertToArrayList(int[] num) {
      ArrayList<Integer> list = new ArrayList<Integer>(); // Using wrapper class
                                                          // to wrap/store primitive objectds
      for(int n: num) {       
         list.add(n);
      }
      splitNum(list);
      System.out.println(list);
   } 
   
   public static void splitNum(ArrayList<Integer> list) {
      for(int i = 0; i < list.size(); i+=2) {
        int number = list.get(i) / 2;
        list.set(i, number + list.get(i) % 2);
        list.add(i + 1, number);
      }
   }
}   