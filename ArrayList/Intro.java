import java.util.*;
import java.io.*;

public class Intro {
   public static void main(String[] args) throws FileNotFoundException {
      Scanner input = new Scanner(new File("simple.txt"));
      ArrayList<String> list = new ArrayList<String>();
      addValues(input, list);
      //int length = list.size();
     // removeDup(list);
      //list.remove(0);
      //removeAll(list, "happy");
      System.out.println(removeDup(list));
      //replace(input, list, "happy", "VeryHappy");
      //System.out.println(list);
      
   }
   
   public static void addValues(Scanner input, ArrayList<String> list) {
      while(input.hasNext()) {
         String token = input.next();
         list.add(token);
      }
   }
   
  
   
   public static void removeAll(ArrayList<String> list,String target) {
      for(int i = list.size() - 1; i >= 0 ; i--) {
         if(list.get(i).equals(target)) {
            list.remove(i);
         }  
      }
   }
   
   // not working
  //  public static void removeDup(ArrayList<String> list) {
//       for(int i = list.size() - 1; i >= 0 ; i--) {
//          String item = list.get(i);
//          for (int j = list.size() - 2; j >= 0; j=- 2) {
//             if(list.get(j).equals(item)) {
//                list.remove(j);
//             }   
//          }   
//       }
//    }

   // Remove duplicates using a new arraylist
   public static ArrayList<String> removeDup(ArrayList<String> list) {
      ArrayList<String> newList = new ArrayList<String>();
      for(int i = 0; i < list.size(); i++) {
         String item = list.get(i);
         if(!newList.contains(item)) {
            newList.add(item);
         }   
      }
      
      return newList;
   }
   
   // remove a string with a resultant string
   public static void replace(Scanner input, ArrayList<String> list, String target, String resultant) {
      while(input.hasNext()) {
         String token = input.next();
         if(token.equals(target)) {
            int index = list.indexOf(token);
            list.set(index, resultant);
         }
      }      
   }
}   