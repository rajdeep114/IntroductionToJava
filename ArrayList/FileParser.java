import java.util.*;
import java.io.*;

// Notes:
// translation from array to ArrayList:
//     String[]          => ArrayList<String>
//     new String[10]    => new ArrayList<String>()
//     a.length          => list.size()
//     a[i]              => list.get(i)
//     a[i] = value;     => list.set(i, value);
// new operations:
//     list.remove(i);     --remove the ith value
//     list.add(value);    --appends a value
//     list.add(i, value); --adds at an index
//     list.clear()        --remove all value
//     list.toString();    --nice String of the list

// Wrapper classes:
//     int -> Integer
//     double -> Double
//     boolean -> Boolean
//     char -> Character

public class FileParser {
    public static void main(String[] args) throws FileNotFoundException {
	// array version of parsing a file
	String[] linesArray = new String[1000];
        Scanner input = new Scanner(new File("data.txt"));
        int lineCount = 0;
        while (input.hasNextLine()) {
            String line = input.nextLine();
            linesArray[lineCount] = line;
            lineCount++;
        }         
	// this loop cannot use linesArray.length as the upper bound
	for (int i = 0; i < lineCount; i++) {
            System.out.println(linesArray[i]);
        }
	System.out.println();

	// ArrayList version of parsing a file
        ArrayList<String> linesList = new ArrayList<String>();
	// reset Scanner at the beginning of a data file
        input = new Scanner(new File("data.txt"));
        while (input.hasNextLine()) {
            linesList.add(input.nextLine());
        }
        for (int i = 0; i < linesList.size(); i++) {
	    System.out.println(linesList.get(i));
	}
	System.out.println();
        
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(5);
        list.add(-1);
        list.add(0);
        System.out.println(list);

    }
}