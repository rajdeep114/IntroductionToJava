import java.util.*;

public class ArrayIntList {
   
   /*       **** FIELDS/STATES ****       
   Define data contained in an object. 
   */
   
   //  Encasuplation of fields
   private int[] element;
   private int size;
   
   // Class constants
   public static final int Default_Capacity = 100;
   
   
   
   /*       **** CONSTRUCTORS ****        
   Initialize the value of fields at the time 
   they are created. 
   */
   
   // Declare constructor with no parameters 
   public ArrayIntList() {
      this(Default_Capacity);
   }
   
   // Declare constructor with parameters
   public ArrayIntList(int capacity) {
      if(capacity < 0) {
         throw new IllegalArgumentException("capacity: " + capacity);
      } 
      this.element = new int[capacity];
      this.size = 0;
   }
   
   
   
   /*            **** MUTATORS ****
   Declare Mutator: instance methods that modifies the content
   of field or state of an object. 
   */
   
   // Add at the end of list
   public void add(int value) {
      element[size] = value;
      size++;  
   }
   
   // Add value at a particular list
   public void add(int index, int value) {
      verifyIndex(index);
      ensureCapacity(size + 1);
      
      for(int i = size - 1; i >= index; i--) {
         element[i + 1] = element[i];
      }
      element[index] = value;
      size++;
   }
   
   // Remove element from any index
   public void remove(int index) {
      verifyIndex(index);
      for(int i = index; i < size - 1; i++) {
         element[i] = element[i + 1];  
      }
      size --;
   }
   
   // Clears up the array
   public void clear() {
      size = 0;
   } 
   
   // Reduce size of list 
   public void collapse() {
      for(int i = 0; i < size / 2; i++) {
         element[i] += element[i + 1];
      }
      if(size % 2 == 0) {
         size = size / 2;
      } else {
         element[size / 2] = element[size - 1];
         size = size / 2 + 1;
      }   
   } 


  
   /*        **** ACCESSORS ****    
   Accessor method: Instane methods that do not modify 
   state of the OBJECTS. Returns information about them
   */
   
   // Returns index of a value
   public int indexOf(int target) {
      for(int i = 0; i < size; i++) {
         if(target == element[i]) {
            return i;
         }
      }
      return -1;
   } 
   
   // Returns size of array list
   public int size() {
      return size;
   }   
   
   // Returns value at particular index
   public int get(int index) {
      verifyIndex(index);
      return element[index];
   }
   
   
   
   /*    **** CONVENIENCE METHODS ****                */
   
   // Return true if value exists in array
   public boolean contains(int value) {
      return indexOf(value) >= 0;
   }
   
   // Return true if list is empty
   public boolean isEmpty() {
      return size == 0;
   }  
   
   // Replace and add value at the same index
   public void set(int index, int value) {
      verifyIndex(index);
      element[index] = value;
   }
   
   // Return sum of element between two indexes
   public int sublistSum(int start, int stop) {
      verifyIndex(start);
      if(stop > size) {
         throw new IndexOutOfBoundsException("index: " + stop);
      }
      int sum = 0;
      for(int i = start; i < stop; i++) {
         sum += element[i];
      }
      return sum;
   }
   
     /*    **** PRIVATE METHODS FOR EXCEPTIONS ****     */
   
   // checks if capacity of array is less than its size 
   private void ensureCapacity(int capacity) {
      if(capacity > element.length) {
         int newCapacity = element.length * 2;
         if(newCapacity < capacity) {
            newCapacity = capacity;
         }
         element = Arrays.copyOf(element, newCapacity);
      }   
   }
   
   // checks if the entered index is legal
   private void verifyIndex(int index) {
      if(index < 0 || index >= size) {
         throw new IndexOutOfBoundsException("index: " + index);
      }
   } 
   
   
   /*       **** CONVERT ARRAY INTO A STRING ****           */
   
   // Modifies the toString method of OBJECT class
   public String toString() {
      if(size == 0) {
         return "[]";
      } else {
         String result = "[" + element[0];
         for (int i = 1; i < size; i++) {
            result += ", " + element[i];
         }
         result += "]";
         
         return result;
      }   
   }                           
}   
   

