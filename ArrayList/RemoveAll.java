import java.util.*;
import java.io.*;

public class RemoveAll {
   public static void main(String [] args) {
      String[] data1 = {"a", "b", "c", "d", "a", "c"};
      String[] data2 = {"a", "a", "a", "a", "a"};
      convertToArrayList(data1, "a");
      convertToArrayList(data1, "b");
      convertToArrayList(data1, "c");
      convertToArrayList(data1, "d");
      convertToArrayList(data2, "a");
      convertToArrayList(data2, "b");
     
   }
   // Converts array to arraylist using for-each loop
   // For-each loop takes in a variable name and type. 
   // After using a :, we usually put an array.
   // This variable store a single value for every iteration 
   // We can use this variable to form new list. 
   // We cannot perform any modification to current array using for-each loop
   
   public static void convertToArrayList(String[] s, String target) {
      ArrayList<String> list = new ArrayList<String>();
      for(String word: s) { // for each data value, get a word
         list.add(word);
      }
      
      System.out.println("Original List: " +  list.toString());
      System.out.println("Target Letter: " + target);
      remove(list, target);
      System.out.println("\nFinal List: " +  list + "\n");
      
   }
   
   // Method to remove a target variable
   public static void remove(ArrayList<String> list, String target) {
      for(int i = list.size() - 1; i >= 0; i--) {
         if(list.get(i).equals(target)) {
            list.remove(i);
         }
      }
   }
}   