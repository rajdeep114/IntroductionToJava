public class LetterInventory {

   //    **** FIELDS/STATE ****
   private int[] inventory;
   private int data;
   
   //    **** CLASS CONSTANT ****
   public static final int elements = 26;
   
   //    **** CONSTRUCTORS ****
   public LetterInventory() {
      this("");
   }
   
   public LetterInventory(String s) {
      inventory = new int[elements];
      stringTraversal(s); 
   }
   
   //    **** MUTATORS ****
   
   // Replace the counter of character with new a value
   public void set(char letter, int value) {
      int index = findIndex(letter);
      this.data -= inventory[index]; 
      inventory[index] = value;
      this.data += value;
   }
   
   public LetterInventory add(LetterInventory other) {
      LetterInventory array =  new LetterInventory();
      for(int i = 0; i < elements; i++) {
         array.set((char)(i + 'a'),inventory[i] + other.get((char)(i + 'a')));
      } 
      return array;    
   }
   
  public LetterInventory subtract(LetterInventory other) {
      LetterInventory array =  new LetterInventory();
      for(int i = 0; i < elements; i++) {
         int value = inventory[i] - other.get((char)(i + 'a'));
         if(value < 0) {
            return null;
         } else {
            array.set((char)(i + 'a'), value);
         }   
      }
      return array;     
   }
  
   //    **** ACCESSORS ****
   
   // Returns inventory of a character
   public int get(char letter) {
      int index = findIndex(letter);
      return inventory[index];
   }
   
   // Return sum of elements of inventory
   public int size() {
      return data;
   }
   
   // Return true if inventory is empty
   public boolean isEmpty() {
      return data == 0;
   }
   
   
   public double getLetterPercentage(char letter) {
      int index = findIndex(letter);
      double percent = Math.round(((double) (inventory[index]) / this.data) * 10.0) / 10.0;
      return percent;
   
   }
   //    **** CONVERT OBJECT TO STRING ****
   public String toString() {
      if(data == 0) {
         return "[]";
      } else {
         String result = "[";
         for(int i = 0; i < inventory.length; i++) {
            for(int j = 0; j < inventory[i]; j++) {
               result += (char)(i + (int) 'a'); 
            }
         }
         result += "]";
         return result;
      }      
   }
  
   
   //    **** HELPER METHOD ****
   
   // Returns index of given character
   private int findIndex(char letter) {
      if(letter < 'A') {
         throw new IllegalArgumentException("NOT A LEGAL CHARACTER: " + letter);
      }   
      letter = Character.toLowerCase(letter);
      int index = (int) letter - 'a';
      return index;
   }
   
   // Traverse string passed through the constructor
   private void stringTraversal(String s) {
      s = s.toLowerCase();
      for(int i = 0; i < s.length(); i++) {
         if(s.charAt(i) >= 'a') {
            inventory[s.charAt(i) - 'a']++;
            this.data++;
         }
      }
   }
}   