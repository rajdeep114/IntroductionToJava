public class Lecture_Parameters_return_values{
   public static void main(String [] args){
      
     table(360,30);
     System.out.println();
     table(180,20);
   }
   /*Note: In this exercise we called a static method in other method. This is a very useful tool that eliminates
      redundency and hence, it makes the program very compact. For instance, there are some method, such as 
      Math.round(value), we need to manipulate according to our needs. We make a new method and call the existing
      method in this new method.
      
      Changes in the existing methods can be made by calling these methods and doing the required manipulations. 
      These manipulations are arithematic in nature, however, we can perform any operation we like on the existing
      method. After making the changes to the existing method, we store these method in variable under the new 
      method we created, like roundoff. 
      
      Return is very important tool when we manipulate existing methods. We use the return command to get an 
      output from the methods we manipulated. Without the return command, the new method would not work and the 
      program might not even compile.
      
      */
   public static void table(int max, int increments){
      System.out.println("n\tsin(n)\tcos(n)");
      int iteration = max/increments;
      
      for( int i =  0; i <= iteration; i++){
         double angle = Math.toRadians(i * increments);
         double sinvalues =  roundoff(Math.sin(angle));
         double cosvalues =  roundoff(Math.cos(angle));
         System.out.println(i * increments + "\t" + sinvalues + "\t" + cosvalues );
         
      }
   }
   
   public static double roundoff(double values){
      double round = Math.round(100.0 * values)/100.0;
      return round;
   }
      
}      