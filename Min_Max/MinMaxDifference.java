import java.util.*;

public class MinMaxDifference{
   public static void main(String [] args){
      Scanner console = new Scanner(System.in);
      int range = findRange(console); 
      System.out.println(range); 
      
   }
   
   public static int findRange(Scanner console){
      System.out.print("Please enter a number(zero to quit)");
      int number = console.nextInt();
      int max = number;
      int min = number;
      int range = 0;
      while(number != 0){
         if (number < min){
            min = number;
         } 
         
         if (number > max){
            max = number;
         }   
         range = max - min;
         System.out.print("Please enter a number(zero to quit)");
         number = console.nextInt();
      }
      System.out.println("Minimum number you enter is: " + min); 
      System.out.println("Maximum number you enter is: " + max); 
      return range;   
      
   }
}  
   
      