import java.util.*;

public class SentinalLoop{
   public static void main(String [] args){
      Scanner console = new Scanner(System.in);
      System.out.print("Type a number or enter 0 to quit: ");
      int number = console.nextInt();
      int min = number;
      
      /* When using the while loop, make sure varible use  
         inside the loop are declared outside the while loop. 
         These sentinal loops can be deceptively difficult
         to get the correct output */
      
      while(number != 0){
         if (min > number){
            number = min;
         }
         
         System.out.print("Type a number or enter 0 to quit: ");
         number = console.nextInt();

      }
      
      System.out.print(min);
   
   }

}   
      