public class Rocketship{
   public static final int x = 5;
   public static void main(String[]args){
      ship();
   }
   
   public static void ship(){
      part1();
      solidLine();
      part2();
      part3();
      solidLine();
      part3();
      part2();
      solidLine();
      part1();
      count12();
   }
   
   public static void part1(){
      for(int Line = 1; Line <= 2*x-1; Line++){
         for(int space = 1; space <= 2*x-Line; space++){
            System.out.print(" ");
         }
         for(int forwardslash = 1; forwardslash <= Line; forwardslash++){
            System.out.print("/");
         }
         
         System.out.print("**");
         
         for(int forwardslash = 1; forwardslash <= Line; forwardslash++){
            System.out.print("\\");
         }
         for(int space = 1; space <= 2*x-Line; space++){
            System.out.print(" ");
         }
         
         System.out.println(" ");
      }
   }
  
   public static void solidLine(){
      System.out.print("+");
      for(int i = 1; i <= x*2; i++){
         System.out.print("=*");
      }
      System.out.println("+");
   }
   
   public static void part2(){
      for(int Line = 1; Line <= x; Line++){
      
         System.out.print("|");
         
         for(int dot = 1; dot <= -1*Line+(x); dot++){
            System.out.print(".");
         }
         for(int slash = 1; slash <= Line; slash++){
            System.out.print("/\\");
         }
         for(int dot = 1; dot <= -2*Line+2*(x); dot++){
            System.out.print(".");
         }
         for(int slash = 1; slash <= Line; slash++){
            System.out.print("/\\");
         }
         for(int dot = 1; dot <= -1*Line+(x); dot++){
            System.out.print(".");
         }
         System.out.println("|");
      }
   }
   
   public static void part3(){
      for(int Line = 1; Line <= x; Line++){
      
         System.out.print("|");
         
         for(int dot = 1; dot <= Line-1; dot++){
            System.out.print(".");
         }
         for(int slash = 1; slash <= -1*Line+(x+1); slash++){
            System.out.print("\\/");
         }
         for(int dot = 1; dot <= 2*Line-2*(x-(x-1)); dot++){
            System.out.print(".");
         }
         for(int slash = 1; slash <= -1*Line+(x+1); slash++){
            System.out.print("\\/");
         }
          for(int dot = 1; dot <= Line-1; dot++){
            System.out.print(".");
         }

         System.out.println("|");
      }
   }
   
   public static void count12(){
      for(int j = 1; j <= 3; j++){
         for(int i = 1; i < 10; i++){
            System.out.print(i+""+i);
         }
         System.out.print("00");
      }
   }
   
}