public class Rocketship_parameter{
   public static final int x = 5;
   public static void main(String[]args){
      ship();
   }
   
   public static void ship(){
      part1();
      solidLine();
      part2();
      part3();
      solidLine();
      part3();
      part2();
      solidLine();
      part1();
      
   }
   
   public static void part1(){
      for(int Line = 1; Line <= 2*x-1; Line++){
         writeSpaces(2*x-Line);
         
         forwardslash(Line);
         System.out.print("**");
         backwardslash(Line);
         writeSpaces(2*x-Line);
         
         System.out.println(" ");
      }
   }
  
   public static void solidLine(){
      System.out.print("+");
      for(int i = 1; i <= x*2; i++){
         System.out.print("=*");
      }
      System.out.println("+");
   }
   
   public static void part2(){
      for(int Line = 1; Line <= x; Line++){
      
         System.out.print("|");
         
         dots(-1*Line+(x));
         twoslash(Line);
         dots(-2*Line+2*(x));
         twoslash(Line);
         dots(-1*Line+(x));
         
         System.out.println("|");
      }
   }
   
   public static void part3(){
      for(int Line = 1; Line <= x; Line++){
      
         System.out.print("|");
         dots(Line-1);
         twoslash1(-1*Line+(x+1));
         dots(2*Line-2*(x-(x-1)));
         twoslash1(-1*Line+(x+1));
         dots(Line-1);
        
         System.out.println("|");
      }
   }
   
   public static void count12(){
      for(int j = 1; j <= 3; j++){
         for(int i = 1; i < 10; i++){
            System.out.print(i+""+i);
         }
         System.out.print("00");
      }
   }
   
   public static void writeSpaces(int number){
      for(int i = 1; i <= number; i++){
         System.out.print(" ");
      }
   }     
   
   public static void forwardslash(int number){
      for(int i = 1; i <= number; i++){
         System.out.print("/");
      }
   }
   
   public static void backwardslash(int number){
      for(int i = 1; i <= number; i++){
         System.out.print("\\");
      }
   } 
   
   public static void dots(int number){
      for(int i = 1; i <= number; i++){
         System.out.print(".");
      }
   }
   
   public static void twoslash(int number){
      for(int i = 1; i <= number; i++){
         System.out.print("/\\");
      }
   } 
   
   public static void twoslash1(int number){
      for(int i = 1; i <= number; i++){
         System.out.print("\\/");
      }
   }   
}