public class Rocketship_parameter2{
   public static final int x = 6;
   public static void main(String[]args){
      ship();
   }
   
   public static void ship(){
      part1();
      solidLine();
      part2();
      part3();
      solidLine();
      part3();
      part2();
      solidLine();
      part1();
      
   }
   
   public static void part1(){
      for(int Line = 1; Line <= 2*x-1; Line++){
         Char(' ',2*x-Line);
         Char('/',Line);
         System.out.print("**");
         Char('\\',Line);
         Char(' ',2*x-Line);
         System.out.println(" ");
      }
   }
  
   public static void solidLine(){
      System.out.print("+");
      Char2('=','*',x*2);
      System.out.println("+");
   }
   
   public static void part2(){
      for(int Line = 1; Line <= x; Line++){
      
         System.out.print("|");
         
         Char('.',-1*Line+(x));
         Char2('/','\\',Line);
         Char('.',-2*Line+2*(x));
         Char2('/','\\',Line);
         Char('.',-1*Line+(x));
         
         System.out.println("|");
      }
   }
   
   public static void part3(){
      for(int Line = 1; Line <= x; Line++){
      
         System.out.print("|");
         Char(',',Line-1);
         Char2('\\','/',-1*Line+(x+1));
         Char('.',2*Line-2*(x-(x-1)));
         Char2('\\','/',-1*Line+(x+1));
         Char('.',Line-1);
        
         System.out.println("|");
      }
   }
   
   public static void count12(){
      for(int j = 1; j <= 3; j++){
         for(int i = 1; i < 10; i++){
            System.out.print(i+""+i);
         }
         System.out.print("00");
      }
   }

   public static void Char(char ch,int number){
       for(int i = 1; i <= number; i++){
         System.out.print(ch);
      }
   }
   
   public static void Char2(char ch,char ch2,int number){
       for(int i = 1; i <= number; i++){
         System.out.print(ch+""+ch2);
      }
   }
   
    
}