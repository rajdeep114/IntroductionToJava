public class java_lecture{
   public static final int x = 10;
   public static void main(String []args){
   
   DrawLine();
   TopHalf();
   BottomHalf();
   DrawLine();
   //print_ln();
  
   }
   
   public static void stars(){
      for (int i = 1;i <= 5; i++){
         for (int j = 1; j<= -1*i+5; j++){
            System.out.print(".");
         }
      System.out.println();
      }
   }
   
   public static void DrawLine(){
   
      System.out.print("#");     /* print # sign, 16 = signs, # */
      for(int i = 1; i <= 4*x; i++){
         System.out.print("=");
         }
      System.out.print("#");
   
   }
   
   public static void TopHalf(){
   
   /*
   Line  `spaces     -2* Line    -2* Line + 8      dots     4 * Line - 4      SIZE
   1        6           -2          6               0            0             4
   2        4           -4          4               4            4
   3        2           -6          2               8            8
   
   
   */
      
      for (int Line = 1; Line <= x; Line++){
         System.out.print("|");
         for(int spaces = 1; spaces <=-2* Line + 2*x; spaces++){
            System.out.print(" ");
         }
      
            System.out.print("<>");
      
         for(int dots = 1; dots <=4 * Line - 4; dots++){
            System.out.print(".");
         }
      
         System.out.print("<>");
      
         for(int spaces = 1; spaces <=-2* Line + 2*x; spaces++){
            System.out.print(" ");
         }
         System.out.println("|");
         
      }
   }
  
   
   public static void BottomHalf(){
   
   for (int Line = x; Line >=1; Line--){
         System.out.print("|");
         for(int spaces = 1; spaces <= -2* Line + 2*x; spaces++){
            System.out.print(" ");
         }
      
            System.out.print("<>");
      
         for(int dots = 1; dots <= 4 * Line - 4; dots++){
            System.out.print(".");
         }
      
         System.out.print("<>");
      
         for(int spaces = 1; spaces <= -2* Line + 2*x ; spaces++){
            System.out.print(" ");
         }
         System.out.println("|");
         
      }
   
   }
   
   public static void print_ln(){
    
      System.out.println("Happy");
      System.out.println("World");
      System.out.print("World_next_line");
   
   }
 }
       