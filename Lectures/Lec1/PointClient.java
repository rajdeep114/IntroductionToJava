//	Alan kay
//	OOP: object	oriented	programming
//	Not been	very successful, in comparison to GUI

//	---> for	DOS, it is verb noun
//	---> for	Mac, it is noun verb
import java.util.*;

public class PointClient {
	public static void main(String[]	args)	{
      Point p1 = new Point(3, 5);
      Point p2 = new Point(3, 5);
      Point p3 = new Point(66, 77);
      // int x = 12;
      // int y = 13;
      
		// p1.x = 3;
//       p1.y = 5;
//       p2.x = 3;
//       p2.y = 5;
//       //translate(p1, 2, 3);
      p1.translate(2, 3);
      
      double distance = p1.distancefromOrigin();
      
      // local method --> translate
      // translate/ moving points by some value
      
      // this allows you to access implicit function
      System.out.printf("%.2f\n", distance);
      // System.out.println(p1.convert());
      
      // toString method in the object class --
      // every object gets access to this class using inheritance
      
      // explicit call to the toString method
      System.out.println(p1.toString());
      
      // implicit call to the toString method
      System.out.println(p2);
      
      System.out.println(p3);
      
      
	}
   
   // *********************************************************
   // Point p in this method to is refering to the same object 
   // in the main. --> OBJECTS ARE PASSED AS REFERENCE SYMETIC
   // *********************************************************
/*
   public static void translate(Point p, int dx, int dy) {
      p.x = p.x + dx;
      p.y = p.y + dy;
   }
*/
}