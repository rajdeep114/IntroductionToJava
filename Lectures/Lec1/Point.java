// collect data together
// state and behaviour
// client vs implementor
// state --> data insige the object, it is also related data
// behavior --> methods
// client --> you are using the object, eg: main.c
// implementor --> knowing how it works


// class --> blueprint of a kind of object, but havent created the objects yet
// intances of a class --> several creation of objects using a the blueprint(class) 
// object --> point after instantiated

// Using Encapsulation --> use private -- all field must be declared as private for 
// practical purpose
public class Point {
   // fields --> what data the object needs ti remember
   
// ** Encapsulation **
   private int x;      // these value stay indefinately
   private int y;

   // constructor
   // this is redundant tho!!
   public Point() {
      // x = 0;
      // y = 0;
      // setLocation(0, 0) or
      this(0, 0);
   }
   
   public Point(int initialX, int initialY) {
      //x = initialX;
     // y = initialY;
     setLocation(initialX, initialY);
   }

// getter and setter
   public int getX() {
      return x;
   }
   
   public int getY() {
      return x;
   }

// instance method -- refers to the field
// static word does not allow other files to link
// to the methods in the static class.

// non static class lets other files access methods
// in the non-static class

   public void translate(int dx, int dy) {
      x = x + dx;   
      y = y + dy;
   }
   
   
   // shadowing 
   public void setLocation(int x, int y) {
     // x = x;   // set parameter x to parameter x
     // y = y;   // set parameter y to parameter y
     this.x = x;
     this.y = y;
   }
   
   public double distancefromOrigin() {
      return (((Math.sqrt(x * x + y * y)) * 100) / 100);
   }
   
   // We are over writing the toString method provided by the object class
   // public String convert() {
   public String toString() {
      return "(" + x + ", " + y + ")";
   }

}
