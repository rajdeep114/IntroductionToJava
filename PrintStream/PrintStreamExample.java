import java.io.*;

public class PrintStreamExample {
   public static void main(String[] args) throws FileNotFoundException {
      PrintStream stream = new PrintStream("out2.txt"); // Send output to a file named out2.txt
      //stream = System.out; // System.out is just a special kind of PrintStream that
                              // directs output to the console!
      stream.println("Hellojkjkjkjjjkjkjkjkjkjkkjk!");
      stream.println("This is a line of text");
   }
}