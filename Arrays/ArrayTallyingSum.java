import java.util.*;

public class ArrayTallyingSum {
   public static void main(String [] args) {
      int[] a = {7, 8, 3, 4, 9, 2, 5, 8, 13, 7};
      int[] b = {3, 5, 2, 9, 8, -4, 7, 6, 2, 8, 3, 1, 0, 42, 12};
      int[] c = {1, 3, 8, 9, 5, 7, 2};
      int[] d = {3, 8};
      
      // print different arrays
      print(a,sum5(a));
      print(b,sum5(b));
      print(c,sum5(c));
      print(d,sum5(d));
      
   }
   // Method to print original and summation/modified array
   public static void print(int[] a, int[] b) {
      System.out.println("This is original array: " + Arrays.toString(a));
      System.out.println("This is modified array: " + Arrays.toString(b) + "\n");
   }
      
   // Method return sum of every fifth element(1 + 6 + 11 ....)
   public static int[] sum5(int[] array) {
      int[] sum;
      if(array.length >= 5){
         sum = new int[5];
      } else {
         sum = new int[array.length];
      }      
      for(int i = 0; i < array.length; i++){
         sum[i % 5] += array[i];
      }
      return sum;
   }
}   