import java.io.*;
import java.util.*;
public class ArrayReverse{
   public static void main(String [] args){
      int a = 7;
      int b = 35;
      int temp = a;
      a = b;
      
      b = temp;
      
      System.out.println(a + "  " + b);
      
      int[] a1 = {11, 42, -5,27, 0, 89};
      System.out.println(Arrays.toString(a1));
      swapFirst(a1);
      System.out.println(Arrays.toString(a1));
      
      int[] a2 = a1;
      a2[0] = 7;
   }
   // if we use a.length, it swaps all the elements twice
   // and therefore we get the same array back.
   public static void swapFirst(int[] a){
      for(int i = 0; i < a.length/2; i++){
         int temp = a[i];
         a[i] = a[a.length - 1- i];
         a[a.length - 1- i] = temp;
      }   
   
   }
   
}     