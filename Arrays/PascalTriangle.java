import java.util.*;

public class PascalTriangle {
   public static void main(String[] args) {
      Scanner console = new Scanner(System.in);
      System.out.print("How many rows for triangle do you want? ");
      int rows = console.nextInt();
      int[][] triangle = new int[rows][];      // Used to create a jagged array stucture
      drawTriangle(triangle);
      printTriangle(triangle);
      
   }
   
   public static void drawTriangle(int[][] triangle) {
      for(int i = 0; i < triangle.length; i++) {
         triangle[i] = new int[i+1];
         triangle[i][0] = 1;
         triangle[i][i] = 1;
         for(int j = 1; j < i; j++) {
            triangle[i][j] = triangle[i - 1][j - 1] + triangle[i - 1][j];
         } 
      
      }
      //return triangle;
   }
   
   public static void printTriangle(int[][] triangle) {
      for(int i = 0; i < triangle.length; i++) {         // print rows
         for(int j = 0; j < triangle[i].length; j++) {   // print columns
            System.out.print(triangle[i][j] + " ");
         }
         System.out.println();
      }
   } 
}   
      