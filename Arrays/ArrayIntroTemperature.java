import java.util.*;

public class ArrayIntroTemperature{
   public static void main(String [] args){
      Scanner console = new Scanner(System.in);
      System.out.print("How many days' temperatures? ");
      int days = console.nextInt();
      int[] temp = new int[days];
      double averageTemp = averageTemp(console, days, temp);
      minimumTemp(temp, averageTemp);
   }
   
   public static double averageTemp(Scanner console, int days, int[] temp){
     
      int sum = 0;
      for(int i = 0; i < days; i++){
         System.out.print("Day " + (i + 1) + "'s high temp: ");
         int response = console.nextInt();
         temp[i] = response;
         sum += temp[i];
      }
      
      System.out.println("\nAverage temp = " + (double)Math.round((10 * sum)/days)/10);
      //System.out.println(Arrays.toString(temp)); 
      return (double)Math.round((10 * sum)/days)/10;
   }
   // Find mininmum temperature, number of days above average and 
   // Report two minimum and maximum temp using sort
   public static void minimumTemp(int[] temp, double averagetemp){
      int minTemp = (int)averagetemp;
      int count = 0;
      for(int i = 0; i < temp.length ; i++){
          if(temp[i] < minTemp){
             minTemp = temp[i];
          }
         if(temp[i] > averagetemp){
            count++;
         }   
      }
      Arrays.sort(temp); // sort method for arrays
      System.out.println(Arrays.toString(temp));
      System.out.println("There were " + count + " days above average");
      System.out.println("Minimum temperature was " + minTemp);
   }
}      
              
      