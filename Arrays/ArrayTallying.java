import java.util.*;
import java.io.*;

public class ArrayTallying {
   public static void main(String [] args) throws FileNotFoundException{ 
      Scanner file = new Scanner(new File("sections.txt"));
      PrintStream s = new PrintStream("Result Sections.txt");
      int section = 1;
      
      // This loop gors through all the lines in the file
      while(file.hasNextLine()) {
         String line = file.nextLine();
         int [] points = countPoint(line);
         double [] grades = grades(points);
         result(s, points, grades, section);
         section++;
      }   
   }
   // Print output in an external file
   public static void result(PrintStream s, int[] points, double[] grades, int section){
      s.println("Section: " + section);
      s.println("Student points: " + Arrays.toString(points));
      s.println("Student grades: " + Arrays.toString(grades)+"\n");
   }
   //Method counts the number of points earned    
   public static int[] countPoint(String line) {
      int[] count = new int[5];
      for(int i = 0; i < line.length(); i++) {
         int student = i % 5;
         char rollCall = line.charAt(i);
         if(rollCall == 'y') {
            count[student] += 5;
            count[student] = Math.min(30, count[student]);
         }
      }
      return count;
   }
   // Method calculates grades earned.
   public static double[] grades(int[] points) {
      double[] grades = new double[points.length];
      for(int i = 0; i < points.length; i++) {
         grades[i] = Math.round(1000.0 *points[i] /30)/10.0;
      }
      return grades;
   }
}   