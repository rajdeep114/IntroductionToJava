import java.util.*;

public class ArrrayTraversal{
   public static void main(String [] args){
      int [] x = {5, 3, 4, 5, 6, 7, 3, 5, 4, 5, 6, 7, 7, 8, 7, 4, 5, 5, 3, 3, 5, 6, 3, 4};
      int [] y = { };
      Arrays.sort(y);
      System.out.println(Arrays.toString(y));
      numUnique(y);
   }
   // This method idntifies the number of unique number it has seen.
   // It takes in a sorted list and then use array traversal to make 
   // comparison between adjacent elements.
   
   public static void numUnique(int[] array){
      int count = 1;
      if(array.length == 0){
         count = 0;
      } else { 
         for(int i = 0; i < array.length - 1; i++){
            if(array[i] != array[i + 1]){
               count++;
            }
         }   
      }
      System.out.println("There are " + count + " unique numbers");
   } 
}        