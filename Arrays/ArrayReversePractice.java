import java.util.*;

public class ArrayReversePractice {
   public static void main(String [] args) {
      int[] array1 = {11, 42, -5, 27, 0, 89};
      int[] array2 = {0, 1, 2, 3, 4, 5};
      int[] array3 = {01, 11, 21, 31, 41,51};
      
      // Print each array
      System.out.println(Arrays.toString(array1));                            // Print array in original order
      System.out.println(Arrays.toString(array2));                            // Print array in original order
      System.out.println(Arrays.toString(array3));
      
      // Print statements for each method listed below
      System.out.println(Arrays.toString(reverse(array1)));                   // Print array in reverse order
      System.out.println(Arrays.toString(reverseIndexed(array1, 1, 2)));        // Swap array at index 1 and 2
      swapAll(array1, array2);                                                // Interchange elements in two array
      System.out.println("This is new Array2 = " + Arrays.toString(array1));
      System.out.println("This is new Array2 = " + Arrays.toString(array2));
      System.out.println(Arrays.toString(merge(array1, array2))); 
      System.out.println(Arrays.toString(merge3(array1, array2, array3)));                                                  // Merge to arrays
   }
   
   // Method reverse order of array and return it. 
   public static int[] reverse(int [] array) {
      int temp;                                 // Create a temporary variable 'temp' 
                                                // to store value at one index and 
                                                // store its value in a different index.
      for(int i = 0; i < array.length/2; i++) {
         temp = array[i];                        // Store value of ith element in temp variable 
         array[i] = array[array.length - 1 - i]; // Switch ith element with a array.length-1-i element 
         array[array.length - 1 - i] = temp;     // (array.length-1-i) element becomes ith using value
                                                 // stored in temp.
      }                 
      return array;                              // returns reverse array
   }
   
   // Method takes in an array and two indices, return an array with swapped the specified indices 
   public static int[] reverseIndexed(int[] array, int a, int b) {
      int temp;
      for(int i = 0; i < 1; i++){
         temp = array [a];
         array [a] = array[b];
         array[b] = temp;
      } 
      return array;  
   }
   
   //  Method that interchange all element in two array
     public static void swapAll(int[] array1,int[] array2) {
      int temp;
      for(int i = 0; i < array1.length; i++){
         temp = array1[i];
         array1[i] = array2[i];
         array2[i] = temp;
      }   
   }
   
   // Merge to merge two arrays
   public static int[] merge(int [] a, int[] b) {
      int newLength = a.length + b.length;
      int[] array = new int[newLength];
      for(int i = 0; i < a.length; i++) {
         array[i] = a[i];
      }
       for(int i = 0; i < b.length; i++) {
         array[a.length + i] = b[i];
      }
      return array;
   } 
   
   // Method to merge three/mutiple arrays.
   public static int[] merge3(int[] a, int[]b, int[] c){
      return merge(merge(a,b), c);  // This is DOPE!!! :)
   }   
}       
