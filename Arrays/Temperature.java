import java.util.*;

public class Temperature{
   public static void main(String [] args){
      Scanner console = new Scanner(System.in);
      
      System.out.print("Days? ");
      int days = console.nextInt();
      
      int total = 0;
      
      int[] temp = new int[days];
      
      for (int i = 1; i <= days; i++){
         System.out.print("Days" + i + "'s high temp? ");
         temp[i - 1] = console.nextInt();
         
         total += temp[i - 1];
      }
      
      double average = (double) total/days;
      System.out.println("average " + average);
      
      int count = 0;
      for(int i = 0; i < days; i++){
         if(temp[i] > average){
            count++;
         }
      } 
      
      System.out.println("There are " + count + " days above average"); 
      System.out.print(Arrays.toString(temp)); // prints an array in the console       
      
     } 
      
  }
      