import java.util.*;

public class ArrayIntro{
   public static void main(String [] args){
      int[] array = new int[10];
      
      // assign value to array
      array[3] = 5;
      array[6] = 2;
      array[7] = 17;
      
      System.out.println(array[6]); // return value from array
      
      for (int i = 0; i < array.length; i++){  // Ten element in the array
                                                // array length is without brackets
         array[i] = 42;
         array[i]++;
      }   
      
      int x = 0;
      x = 4;
      array[8] = 4;
      
      System.out.println(2 * x + 1);
      System.out.println(2 * array[8] + 1);
      
      x++;
      array[8]++;
   
      double[] doubleArray = new double[5];
      boolean[] booleanArray = new boolean[5]; // filled with false
      String[] stringArray = new String[5]; //nothing null
      
      
      int[][] crazyArray = new int[4][10];// array inside of a array
      
   //    String[] Groceries = new String[5];
//       Groceries[0] = "eggs";
//       Groceries[1] = "apple";
//       Groceries[0] = "banana";
      
               // or
      // quick initialize of array
      String[] Groceries = {"eggs", "apple","banana"};//new String[5];
      Groceries[0] = "pineapple"; // overwrite eggs
      // we cannot change length of an array or delete an element from array
      //Groceries[0] = "eggs";
      //Groceries[1] = "apple";
      //Groceries[0] = "banana";
            
   }
}      