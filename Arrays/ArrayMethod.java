import java.util.*;
public class ArrayMethod{
   public static void main(String [] args){
      int[] a = {42, 17, 30, 2};
      int[] b = {10, 51, 26};
      System.out.println("The sum of a is " + sum(a));
      //int[] result = concatenate(a,b);
      System.out.println(Arrays.toString(concatenate(a,b)));
   }
   
   public static int sum(int[] a){
      int sum = 0;
      for (int i = 0; i < a.length; i++){
         sum += a[i];
      }
      
      return sum;
   }
   
   public static int[] concatenate(int[] a, int[] b){
      int newlength = a.length + b.length;
      int [] ab = new int[newlength];
      
      for(int i = 0; i < a.length; i++){
         ab[i] = a[i];
      }
            
      for(int i = 0; i < b.length; i++){
         ab[a.length + i] = b[i];
      }
      
      return ab;
      
   }   
}         
         