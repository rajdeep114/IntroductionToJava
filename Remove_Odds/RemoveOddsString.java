import java.util.*;

public class RemoveOddsString{
   public static void main(String [] args){
      Scanner console = new Scanner(System.in);
      System.out.print("Please enter a number: ");
      String response = console.next(); 
      removeodds(response);   
      
   }
   // This method remove odds from a string
   public static void removeodds(String s){
      int iter = s.length() - 1;
      int result = 0;
      int place = 1;
      for(int i = iter ; i >= 0 ; i--){
         String digit = s.substring(i,i+1);
         int x = Integer.parseInt(digit);
         if(x % 2 == 0){
            result = result + (x * place);
            place = place * 10;
         }  
      }
      System.out.print(result);   
   }
}           