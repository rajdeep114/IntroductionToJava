import java.util.*;

public class OddsRemove{
   public static void main(String [] args){
      Scanner console = new Scanner(System.in);
      System.out.print("Please enter a number: ");
      int response = console.nextInt();
      System.out.print(removeOdds(response));
   }
   
   public static int removeOdds(int n){
      int result = 0;
      int place = 1;
      int digit  = 2323;
      
      while(n > 0){
          digit = n % 10; // 3859432
         if (digit % 2 == 0){
            result = result + (digit * place);
            place = place * 10;
         }
         n = n / 10;
      }
      return result;
   }
}            