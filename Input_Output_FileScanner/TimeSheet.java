import java.util.*; // Scanner
import java.io.*;    // file scanner


public class TimeSheet{
   public static void main(String [] args) throws FileNotFoundException{
      Scanner file = new Scanner(new File("Time Sheet.txt"));// Scans entire file
      Scanner console = new Scanner(System.in);
      System.out.print("Enter Id: ");
      int desiredId = console.nextInt();
      hours(file, desiredId);
   }
   // This method displays total number of hours worked
   // along with number and Id of the employess.
   public static void hours(Scanner input, int desiredId){
      boolean found = false;
      while(input.hasNext()){          // performs test on individual line
         String text = input.nextLine();  // Stores individual line as string
         Scanner line = new Scanner(text);// Scanner for each token in a single line
         int ID = line.nextInt();         // Stores first token i.e. ID
         if (ID == desiredId){
            found = true;
            String Name = line.next();       // Stores second token i.e. Name
            int days = 0;                    // initiate number of days worked
            double sum = 0;                  // initiate sum of days worked
            while(line.hasNextDouble()){     // loop that goes through each token of single line
               double token = line.nextDouble();
               sum += token;
               days++;
            }   
            System.out.println(Name + " worked " + (double)Math.round(10 * sum) / 10 + " hours " + 
                          "("  + (double)Math.round(100*sum/days)/100 + " hours/day)");    
         }   
      }
      if (found == false){
         System.out.println("No such Id exists");   
      }     
   }
}   