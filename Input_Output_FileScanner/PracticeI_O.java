import java.io.*;
import java.util.*;

public class PracticeI_O{
   public static void main(String [] args)throws FileNotFoundException{
      Scanner file = new Scanner(new File("Statement of Purpose.txt")); // scanning a file
      expandFile(file);                                   
   }
   
   public static void expandFile(Scanner input){
      
      while(input.hasNext()){
         String text = input.nextLine();
         if(text.length() > 0 && text.charAt(0) == '.'){
            text = text.substring(1).toUpperCase();
            text = text.replace("SCHOOL","GOOL");
            System.out.println(text);
            for(int i = 1; i <= text.length(); i++){
               System.out.print("-");
            }
            System.out.println();   
         } else{
               System.out.println(text); 
         }  
      }
      
   }
}      
     