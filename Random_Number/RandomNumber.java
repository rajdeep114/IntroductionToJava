import java.util.*;

public class RandomNumber{
   public static void main(String [] args){
      System.out.println("Let's roll some dice !!");
      Random r = new Random();
      int count = 0;
      int timesodd = 0;
      
      /* If want to quit while loop when there are 3 odd numbers printed simuntaniously,
         we use a if and else statement, which increments if number is odd and decrement 
         if number is even. Our program exits the while loop when timesodd < 3 */
      while(timesodd <= 3){
         int random_number = r.nextInt(6) + 1;
         
         if (random_number % 2 == 1){
            timesodd++;
         }else{
            timesodd = 0;
         }
         System.out.println("You rolled : " + random_number);  
      }
         
   }  
}
     