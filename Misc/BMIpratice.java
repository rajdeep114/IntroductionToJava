public class BMIpratice{
   public static void main(String []args){
      double height = 55;
      double width  = 99;
      int bmi    = (int)((height*width)/90);/* In this activity we learned that we need to initialize a variable and then
                                               later we assign a value to that variable. This is important to understand
                                               that the variable "get's" or "is assigned" a value. It is not an equality
                                               relationship like algebra. We also examine the use of casting i.e. going 
                                               between data types. We understood how to use the %. Increment and decrement 
                                               is very important when we are working with the for loop. */
      
      double x;
      x = (height*100)/width;
      
      System.out.println("x = "+(int)x);
      x++;
      x+=50;
      
      
      System.out.println("My BMI is "+bmi+" "+(int)x);
   }
}