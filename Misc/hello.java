public class hello{
   public static void main (String args[]){
      System.out.println("HELLO World");
      System.out.print("\t Hi how r u ??\n\t I am good. \n\t How is school?\n\t It is \"Great\"\n \n");
      repeat();
      repeat();
      repeat();
      
      }
   public static void repeat(){
   /* This approach shows that you can use multiple static method
    in order to shorten your program. In this case we used a total
    of four static method to obtain the desired result.The best way
    to analyse programs like this is that you can break the figure
    into smaller portion that are repeating. Then use multiple 
    methods to obtain the design for the repeating portion by using 
    several method. After doing this you will obtain a list of methods.
    Since all these methods are static, you can combine all these methods
    to obtain a single method and then call this method multiple times to
    get the desired result. */
      line();
      string();
      opp_line();
      }
   public static void line(){
      System.out.println("////////////////////");
      }
   public static void opp_line(){
      System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
      }
   public static void string(){
      System.out.println("|| Victory is mine ||");
      }
    }