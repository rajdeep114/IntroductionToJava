public class DateClient {
   public static void main(String [] args) {
      Date d1 = new Date(2, 3, 2014);
      Date d2 = new Date(3, 4, 2011);
      Date d3 = new Date(d1);
      Date d4 = new Date(2,2,2012);
      
      d1.setDate(3,2,2012);
      System.out.println("Date is: " + d1);
      System.out.println("Is it a leap year? : " + d1.isLeapYear());
      System.out.println("How many days in year of " + d1 + "? " + d1.getDaysInYear());
      System.out.println("How many days in month of " + d1 + "? " + d1.getDaysInMonth() + "\n");
      
      d2.setDate(2,2,2011);
      System.out.println("Date is: " + d2);
      System.out.println("Is it a leap year? : " + d2.isLeapYear());
      System.out.println("How many days in year of " + d2 + "? " + d2.getDaysInYear());
      System.out.println("How many days in month of " + d2 + "? " + d2.getDaysInMonth());
      
      d3.setDate(3,2,2012);
      System.out.println("Date is: " + d3);
      System.out.println("Is it a leap year? : " + d3.isLeapYear());
      System.out.println("How many days in year of " + d3 + "? " + d3.getDaysInYear());
      System.out.println("How many days in month of " + d3 + "? " + d3.getDaysInMonth() + "\n");
      
      //d2.setDate(2,2,2011);
      System.out.println("Date is: " + d4);
      System.out.println("Is it a leap year? : " + d4.isLeapYear());
      System.out.println("How many days in year of " + d4 + "? " + d4.getDaysInYear());
      System.out.println("How many days in month of " + d4 + "? " + d4.getDaysInMonth());
      
   }
}