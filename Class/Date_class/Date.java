public class Date {
   
   // FIELDS: which are the data/ state of an object
   private int day;
   private int month;
   private int year;
   
   // CONSTRUCTOR: Assign initial values to an object at the 
   // time of creation.
   public Date(int day, int month, int year) {
      setDate(day, month, year);
   }
   
   public Date(Date other) {
      this(other.day, other.month, other.year);
   }
   
   public Date() {
      this(0, 0, 0);
   }   
   
   // Mutator
   public void setDate(int day, int month, int year) {
      setDay(day);
      setMonth(month);
      setYear(year);
   }
   
   public void setDay(int day) {
      if( day < 1 || day > getDaysInMonth()) {
         throw new IllegalArgumentException("Value not valid = " + day);
      }  
         this.day = day; 
   }
   
   public void setMonth(int month) {
      if(month < 1 || month > 12) {
         throw new IllegalArgumentException("Value not valid = " + month);
      }  
         this.month = month; 
   }
   
   public void setYear(int year) {
      if( year < 1 ) {
         throw new IllegalArgumentException("Value not valid = " + year);
      }  
         this.year = year; 
   }
   
   //Accesor 
   
   // Return number of days, month, years
   
  public int getDay() {    
      return day; 
  } 
   
    public int getMonth() {    
      return month; 
   }
   
   public int getYear() {    
     return year; 
   }
   
   public int getDaysInMonth() {
      if( month == 4 || month == 6 || month == 9 || month == 11) {
         return 30;
      } else if (month == 2) {
      
      // call leap year method
         if(isLeapYear()) {
            return 29;
         } else {
            return 28;
         }      
            
      } else {
         return 31;
      }          
   } 
   
   public int getDaysInYear() {
      if(isLeapYear()) {
         return 366;
      } else {
         return 365;
      }
   }         
   
   // Method to find leap year
   public boolean isLeapYear() {
    //   if( year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
//          return true;
//       } else { 
//          return false;
//       }
      return(year % 400 == 0 || (year % 4 == 0 && year % 100 != 0));
   } 
   
   // return object as a string
   public String toString() {
      return month + "/" + day + "/" + year ;
   }          
}