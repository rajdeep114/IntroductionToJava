public class Point {
   // define fields, which holds the state of point object
   // Scope of these fields are the entire class, since they declared directly in the class
   
   int x;
   int y;
   
   // Create behaviour using intance methods
   // There are two types of instance method, namely mutator(set type) and accessor(get type)
   
   // Methods that modify the contents of a field is called mutator. There return type is void
   
   public Point(int x, int y) {
      this.x = x;
      this.y = y;
   }
   
   public void translate(int dx, int dy) {
      x += dx;
      y += dy;
   } 
   
    // Methods that do not modify the contents of a field and gives info about their state
    //  is called accessor. There return type is not void.
    
   public double distFromOrigin() {
      return Math.sqrt(x * x + y * y);
   }
   
   // Return objects(field values) as a string
   
   public String toString() {
      return "(" + x + ", " + y + ")";
   }    
}   