public class PointMain {
   public static void main(String[] args) {
      // contruct new objects
      Point p1 = new Point(1, 2);       // Intantiate an instance of a class called an OBJECT.
      // p1.x = 1;
//       p1.y = 2;
      
      Point p2 = new Point(3, 4);
      // p2.x = 3;
//       p2.y = 4;
      
      print(p1.x, p1.y, p2.x, p2.y);
 
      // Translate using dx and dy values
      p1.x += 1;
      p1.y += 2;
      p2.x += 3;
      p2.y += 4;
      
      print(p1.x, p1.y, p2.x, p2.y);
      
      // Translate using instance method declare inside point class.
      // Each object has a copy of this instance method.
      // ALso instance methods are not static.
      
      p1.translate(1,2);
      p2.translate(3,4);
      
      print(p1.x, p1.y, p2.x, p2.y);
      
      System.out.println("Dist from origin p1 = " + p1.distFromOrigin()+ "\n" +
                        "Dist from origin p2 = " + p2.distFromOrigin());
      
      /* These lines uses toString method that converted object to a string
         toString method is used automatically whenever a onject is concatenated with a string.
         toString method is also implicitly used to convert object to string when 
         using print statement. */ 
                      
      System.out.println("Point p1 is " + p1); 
      System.out.println("Point p1 is " + p2);                 
      
   }
   
   public static void print(int x, int y, int z, int a) {
      System.out.println("p1.x = " + x + "\n" + "p1.y = " + y + "\n" +
      "p2.x = " + z + "\n" + "p2.y = " + a + "\n");
   }   
}      