public class PointClient{ // Client programs are which uses instances of objects
   public static void main(String [] args){
      Point p1 = new Point();
      Point p2 = new Point();
     // Point p3 = p2;
      
      p1.x = 5;
      p1.y = 10;
      
      //p2 and p3 refer to the same instance of class point.
      // This means the refer to same object.
      p2.x = 3;
      p2.y = 6;
      //p3.x = 90;
      
     //  p1.x += 2;
//       p1.y += 4;
//       
//       p2.x += 5;
//       p2.y += 10;
      
      //translate(p1, 2, 4);
      //translate(p2, 5, 10);
      
      p1.translate(2, 4); // P1 is implicite parameter 
      p2.translate(5, 10);// 
      System.out.println("p1 = " + p1.x + ", " + p1.y);
      System.out.println("p2 = " + p2.x + ", " + p2.y);
      //System.out.println("p3 = " + p3.x + ", " +  p3.y);
   }
   
   // This not good, since for every object we need to make this method.
   public static void translate(Point p, int dx, int dy){
      p.x += dx;
      p.y += dy;
   
   }
}   