public class Point{
   // Field: it is a variable inside of an object.
   // Each instance of an object has its own copy.
   // Syntax for field is TYPE NAME;
   int x;
   int y;
   
   public void translate(int dx, int dy){
   
      x += dx; // shift x by dx
      y += dy; // shift y by dy
   }   
}