public class PointTest {
      private int x;
      private int y;
     
     public PointTest(int initialx, int initialy){
      if (initialx >0 ){
      x = initialx;
      y = initialy;
     
     }
     }
     
     public PointTest(){
      x = 0;
      y = 0;
     }
      
      
      public double distance () {
        double distance = Math.sqrt(x*x + y*y);
        return distance;
      }
      
      public String toString(){
         return "(" + x + "," + y + ")";
      
      
      }

}