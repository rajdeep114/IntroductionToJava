import java.awt.*;
import java.util.*;

public class acceleration{
   public static void main(String []args){
      
      DrawingPanel panel = new DrawingPanel(500,500);
      Graphics g = panel.getGraphics();
      g.fillOval(30, 30, 30, 30);
      
      for (double t = 0; t<10; t += 0.09){
         double dis = displacement(1,10,t);
         g.setColor(Color.RED);
         g.fillOval(30,(int) (30 + dis), 30, 30);
         panel.sleep(30);
         panel.clear(); 
        // panel.sleep(200);
      }
   }
   
   
   
   public static double displacement(double v0, double a, double t){
      double displacement = v0 * t + 0.5 * a * Math.pow(t,2);
      return displacement;
   }
}