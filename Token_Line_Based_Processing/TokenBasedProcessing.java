import java.util.*;

public class TokenBasedProcessing{
   public static void main(String [] args){
      // The scanner scans following values
      Scanner input1 = new Scanner("38 4 19 -27 -15 -3 4 19 38");// scanner constructor objects
      Scanner input2 = new Scanner("14 7 -10 9 -18 -8 17 42 98");
      checkNegativeSum(input1);
      checkNegativeSum(input2);
      
   }
   // Used boolean statement to work as a break point in while loop
   //  Using boolean and has next test to solve the problem
   public static void checkNegativeSum(Scanner input){
      int sum = 0;
      boolean foundNegative = false;
      int count = 0;
      while(foundNegative == false && input.hasNextInt()){
         int number = input.nextInt();
         sum += number;
         count++;
         if (sum < 0){
            foundNegative = true;
         }    
      }
      if (foundNegative == true){
         System.out.println("Negative sum after " + count + " steos");
         System.out.println("Sum = " + sum);
      } else {
         System.out.println("No negative sum"); 
      }
      System.out.println();     
   }
}         
      