import java.util.*;

public class Lecture_NestedIfElseStatement{
   public static int Bonus = 10;
   public static void main(String [] args){
      Scanner console = new Scanner(System.in);
      System.out.print(" What is your score? ");
      double score = console.nextDouble();
      Grade3(score);
   
   }
   
   public static void Grade(int score){
   
      if (score >= 90){
         System.out.println(" You got a A :) ");
      }
      
      if (score >= 80){
         System.out.println(" You got a B :) ");
      }
      
      if (score >= 70){
         System.out.println(" You got a C :) ");
      }
      
      if (score >= 60){
         System.out.println(" You got a D :) ");
      }
      if (score < 60){
         System.out.println(" You got a F :( ");
      }
   }
   
   
   public static void Grade2(int score){
   
      if (score >= 90){
         System.out.println(" You got a A :) ");
      } else if (score >= 80){
         System.out.println(" You got a B :) ");
      } else if (score >= 70){
         System.out.println(" You got a C :) ");
      } else if (score >= 60){
         System.out.println(" You got a D :) ");
      }
        else {
         System.out.println(" You got a F :( ");
      }
   }
   
      public static void Grade3(double score){
         if (score >= 3.5){
            System.out.println(" You are on honor roll");
         }
         if (score >= 3.9){
            System.out.println(" You are on Dean list");
         
         }
      }
   
   
}   
   