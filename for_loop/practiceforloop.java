public class practiceforloop{
   public static final int x = 3;
   public static void main(String[]args){
      for(int i = 1; i <= 5;i*=5){           /* for loop is a very important controlled statement which is used in a 
                                                program whenever there is iteration. It has a very simple stucture,which
                                                consist of three types of statement followed by the control statements.
                                                These statements are intialization, continuation test and update.*/
         System.out.println(i+" Happy "+i);
      }
      
      for(int i=1; i <= 10; i++){            /* To understand the basic stucture of the for loop, we must see the result
                                                in the console. By looking at the result we see that the for loop executes
                                                the controlled statement only when the previous update passes the 
                                                continuation test. If the previous update does not pass the continuation
                                                test, then the for loop execution stops, and it moves to the next java 
                                                statement. Only thing worth mentioning here is that the controled statement
                                                executes top to bottom, everytime the previous update passes the continuation
                                                test.*/
                                             
         System.out.println(" ");
         System.out.println(i+" cube is "+(i*i*i));
        
      }
      cone();
      
   }
   
    public static void cone(){
      for(int Line = 1; Line <= x; Line++){
         for(int space = 1; space <= -1*Line+x; space++){
            System.out.print(" ");
         }
         for(int star  =  1; star <= 2*Line-1; star++){
            System.out.print("*");
         }
         for(int space = 1; space <= -1*Line+x; space++){
            System.out.print(" ");
         }
         System.out.println(" ");
      } 
     } 
   
   public static void nestedloops(){
      for(int i = 1; i <= 5; i++){
         for(int j = 1; j <= 3; j++){
            System.out.print("j = "+j+" i = "+i+" ");
         }
         System.out.println(" ");
                                 /*This method has a very important result when we are working with the for loop. If we
                                   we look at the console, we can clearly observe that the inner for loop is the one
                                   that is executed first and then the program goes back to the outer for loop, and thus
                                   repeats that processes. In order to clearly understand how nested loop works, we have
                                   specified the "i"(outer loop) and "j"(inner loop).
                                   If we observe the first line, the inner loop increments by one where as the outer loop
                                   remains the same. Looking horizontally the inner loop increases from 1 to 3 and the 
                                   outer loop remains the same as 1 throughout the first line. Now if we move to the 
                                   second line, the inner loop again increments from 1 to 3, however, the outer loop 
                                   becomes 2 and remains same throughout this line*/
                                 
      }
   }
}