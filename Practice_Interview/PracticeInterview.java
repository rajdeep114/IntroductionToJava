import java.util.*;

public class PracticeInterview {
   public static void main(String[] args) {
   // --> String methods inbuilt
      // s.charAt(int x)
      // s.startsWith("string");
      // s.endsWith("string");
      // s.substring(starts, stop)--> stop is inclusive
      //                              ok to use s.length() with it
      // s.length();
      // s = s.toLowerCase();
      // s = s.toUpperCase();
   
      String s1 = "Hello";
      String s2 = "World!!";
      System.out.println(s1 + " " + s2);
      String combined  = s1 + " " + s2;
     // System.out.println(combined);
     // System.out.println(s1.length());
     // System.out.println(s2.substring(0, s2.length()));
     // printChar(combined);
     // System.out.println(startwith(s1, "2Hel"));
    // System.out.println(s1.indexOf("o"));
     //System.out.println(s1.startsWith("H"));
    // s1 = s1.toLowerCase();
     //System.out.print(s2);
     reverse(combined);
      
      
   }
   
   public static void reverse(String s) {
      String result = "";
      for(int i = 0; i < s.length(); i++)
         result = s.charAt(i) + result;
      System.out.println(result);
   }
   
   public static boolean startwith(String s, String match) {
      for(int i = 0; i < match.length(); i++) {
         if(s.charAt(i) != match.charAt(i))
            return false;
      }
      return true;
   }
   public static void printChar(String s) {
      for(int i = 0; i < s.length(); i++) {
         System.out.println(s.charAt(i));
      }
   }
   
   // Sum of first N integers
   public static int sumOfN(int x) {
      int result = 0;
      while(x != 0) {
         result = result + x;
         x--;
      }  
      return result;
   }
   
   // Pytha thm
   public static double pytha(double a, double b) {
      return Math.sqrt(a * a + b * b);
   }
    
    
   // Factorial
   public static int factorial(int x) {
      int result = 1;
      for(int i = 1; i <= x; i++) 
         result = result * i;
      return result;
   }
   
   // Prints a fill triangle
   public static void Triangle(int size) {
      for(int line = 1; line <= size; line++) {
         forloop(" ", size - line);
         forloop("*", line * 2 - 1);
         System.out.println();
      }
      
   }
   
   // Prints an full inverted triangle 
   public static void invertTriangle(int size) {
      for(int line = 1; line <= size; line++) {
         forloop(" ", line - 1);
         forloop("*", (2 * size + 1) - line * 2);
         System.out.println();
      }
   }
   
   // Prints a square or rect
   public static void square(int x, int y) {
      forloop("* ", x);
      System.out.println();
      for(int i = 1; i < y - 1; i++) {
         System.out.print("*");
         forloop(" ", (2 * x) - 3);
         System.out.print("*");
         System.out.println();
      }
      forloop("* ", x);
      System.out.println();
   }
   
   // Prints squares of integers from 1 to x
   public static void sqr(int x) {
      int result;
      for(int i = 1; i <= x; i++) { 
         result = i * i;
         System.out.println("The square of " + i + " is " + result); 
      }
         
   }
   
   // Prints half traingle
   public static void triangle(int line) {
      for(int i = 1; i <= line; i++) {
         forloop("*", i);
         System.out.println();
      }
   }
   
   // Template to print a string/char multiple times
   public static void forloop(String c, int x) {
      for(int i = 1; i <= x; i++) 
         System.out.print(c);
   }
   
}


     // System.out.println("Hello World");
     // System.out.print("\tHey how are you?\n\t\t");
     // System.out.println("I \"am\" good and you?");
     //square(5,5);
     // int x = 1;
     // x = 3;
     // System.out.println(x);
     // sqr(5);
     //  triangle(10);
     // invertTriangle(8);
     //Triangle(8);
     //System.out.println(factorial(0));
     // System.out.println(sumOfN(100));