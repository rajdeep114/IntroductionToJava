import java.util.*;

public class IntroScanner{
   public static void main (String[]args){
      Scanner console = new Scanner(System.in);
      System.out.println("What is your age and height?");
      int age  = console.nextInt(); // program will not go ahead without any input 
      int height  = console.nextInt();                             // waits for user input
      //Token: it is the user input. Tokens are separated by space
        /*
        Input        No. of token
         23 =              1    
         john smith =      2
         42.0 =            1
         "Hello World" =   2
         $2.50 =           1
         */
                                       
      System.out.println("Your age is "+ age);
      System.out.println("Your height is "+ height);
      
      if ( age >= 21){
         System.out.println("Welcome to the club");
      } else {System.out.println("No Alcohol for you Baby!!");
      
      }
      // if/ else statement are mutually exclusive
     // if (age < 21){
      //   System.out.println(" No Alcohol for you Baby!!");
     // }
      
   }
}
      
    